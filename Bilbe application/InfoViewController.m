//
//  InfoViewController.m
//  Quest For Bible Verses
//
//  Created by Rahul kumar on 1/20/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vmLogoToQuestLabelConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *questLabelToBottomConst;
- (IBAction)contactUsBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@end

@implementation InfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([UIScreen mainScreen].bounds.size.height != 568)
        {
            self.vmLogoToQuestLabelConst.constant = 20.0f;
            self.questLabelToBottomConst.constant = -293.0f;
        }
    }

 }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)requestWebsite:(UIButton *)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://vmokshagroup.com/"]])
    {
        NSLog(@"Failed to open url");
    }
}


#pragma mark
#pragma mark Mail Compose Methods

- (IBAction)moreAppFromDeveloper:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/in/artist/vmoksha-technologies-pvt./id543125173"]];
}

- (IBAction)contactUsBtnAction:(id)sender {
    NSString *subject = @"Quest for Biible verses app Feedback!!";
    NSArray *toRecipents = @[@"mobility@vmokshagroup.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:subject];
    [mc setToRecipients:toRecipents];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        [[UINavigationBar appearance] setTitleTextAttributes:@{UITextAttributeTextColor:[UIColor whiteColor],
                                                               UITextAttributeFont: [UIFont fontWithName:@"Arial" size:15]}];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],
                                                               UITextAttributeFont: [UIFont fontWithName:@"Arial" size:15]} forState:(UIControlStateNormal)];
    } else
    {
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSFontAttributeName: [UIFont fontWithName:@"Arial" size:15]}];
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSFontAttributeName: [UIFont fontWithName:@"Arial" size:15]} forState:(UIControlStateNormal)];
    }

    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(2, 5);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        [[UINavigationBar appearance] setTitleTextAttributes:@{UITextAttributeTextColor:[UIColor whiteColor],
                                                               UITextAttributeTextShadowOffset:[NSValue valueWithCGSize:shadow.shadowOffset],
                                                               UITextAttributeTextShadowColor:shadow.shadowColor,
                                                               UITextAttributeFont: [UIFont fontWithName:@"JamesFajardo" size:33]}];
        
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],
                                                               UITextAttributeFont: [UIFont fontWithName:@"JamesFajardo" size:30]} forState:(UIControlStateNormal)];
    } else
    {
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSShadowAttributeName:shadow ,
                                                               NSFontAttributeName: [UIFont fontWithName:@"JamesFajardo" size:33]}];
        
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSFontAttributeName: [UIFont fontWithName:@"JamesFajardo" size:30]} forState:(UIControlStateNormal)];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
