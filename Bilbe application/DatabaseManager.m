//
//  DatabaseManager.m
//  Bilbe application
//
//  Created by Rahul kumar on 12/31/13.
//  Copyright (c) 2013 Vmoksha. All rights reserved.
//

#import "DatabaseManager.h"

#import "ModelClass.h"

@implementation DatabaseManager
{
    sqlite3 *keywordsDB;
    NSString *dbPath;
    ModelClass *aModel;

}
- (id)init {
    if ((self = [super init])) {
    }
    return self;
}

+ (id)sharedInstance
{
    static DatabaseManager *sharedDatabaseManager = Nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedDatabaseManager = [[DatabaseManager alloc] init];
    });
    return sharedDatabaseManager;
}

- (NSArray *)keyWords
{
    if (!_keyWords1)
    {
        _keyWords1 = [[NSArray alloc] init];
    }
    
    return _keyWords1;
}

- (NSArray *)getTheKeyWordsFromDatabase
{
    dbPath = [[NSBundle mainBundle] pathForResource:@"keywords" ofType:@"db"];
    NSMutableArray *mutableCollectionOfKeyWords = [[NSMutableArray alloc] init];
    
    if (sqlite3_open([dbPath UTF8String], &keywordsDB) == SQLITE_OK)
    {
        NSString *query = @"Select KeyWord FROM KEYWORDS";
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(keywordsDB, [query UTF8String], -1, &statement, Nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *keyWordString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSMutableAttributedString *keyWord = [[NSMutableAttributedString alloc] initWithString:keyWordString];
                [mutableCollectionOfKeyWords addObject:keyWord];
            }
            sqlite3_finalize(statement);
        }else
            NSLog(@"Failed preparetion");
        sqlite3_close(keywordsDB);
    }
    _keyWords1 = mutableCollectionOfKeyWords;
    
    NSLog(@"Keywords is.........%lu",(unsigned long)mutableCollectionOfKeyWords.count);
    
    return mutableCollectionOfKeyWords;
}


-(NSMutableArray *)getverseAndChepterfromDatabase:(NSString *)keyWord
{
    dbPath = [[NSBundle mainBundle]pathForResource:@"AppDataAll" ofType:@"db"];

    NSMutableArray *mainArr = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([dbPath UTF8String], &keywordsDB) == SQLITE_OK)
    {
        
        
        NSString *query =[NSString stringWithFormat:@"SELECT * FROM AppData where KeyWord = '%@'",keyWord];
        
        
       // NSString *query = @"SELECT * FROM AppData where KeyWord = 'A Man'";
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(keywordsDB, [query UTF8String], -1, &statement, Nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
               
                aModel =[[ModelClass alloc]init];
                NSString *chapter = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                 NSString *verses = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                aModel.chapter = chapter;
                aModel.varses = verses;
                [mainArr addObject:aModel];
                
            }
            sqlite3_finalize(statement);
        }else
            NSLog(@"Failed preparetion");
        sqlite3_close(keywordsDB);
    }

    
    return mainArr;

}

-(NSDictionary *)getRandomChapterAndVerse:(int)rowId
{
    NSString *chapter;
    NSString *verses;
    
    NSDictionary *chapterDict =[[NSDictionary alloc]init];
    
    dbPath = [[NSBundle mainBundle]pathForResource:@"AppDataAll" ofType:@"db"];
    if (sqlite3_open([dbPath UTF8String], &keywordsDB) == SQLITE_OK)
    {
        NSString *query =[NSString stringWithFormat:@"SELECT * FROM AppData where rowid = '%d'",rowId];
        
       
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(keywordsDB, [query UTF8String], -1, &statement, Nil) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
              chapter = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
              verses = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            
             chapterDict = @{@"chapter":chapter, @"verse":verses};
            
            }
            
            
            
            
            sqlite3_finalize(statement);
        }else
            NSLog(@"Failed preparetion");
        sqlite3_close(keywordsDB);
    }
    
    return chapterDict;
    
    
}





@end
