//
//  DatabaseManager.h
//  Bilbe application
//
//  Created by Rahul kumar on 12/31/13.
//  Copyright (c) 2013 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseManager : NSObject

@property (strong, nonatomic) NSArray *keyWords1;

+ (id)sharedInstance;

- (NSArray *)getTheKeyWordsFromDatabase;

- (NSArray *)getTheVersesListFromDatabase;


-(NSMutableArray *)getverseAndChepterfromDatabase:(NSString *)keyWord;

-(NSDictionary *)getRandomChapterAndVerse:(int)rowId;


@end
