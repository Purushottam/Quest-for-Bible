//
//  TestTableViewController.h
//  Bilbe application
//
//  Created by Rahul kumar on 11/20/13.
//  Copyright (c) 2013 Vmoksha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "DetailViewController.h"

@interface ResultsListedViewController : UITableViewController <NSXMLParserDelegate,DetailViewControllerDelegate>

@property (strong, nonatomic) NSString *searchQuery;

@property (nonatomic,strong) NSString *keyWord;



@end
