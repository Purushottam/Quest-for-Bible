//
//  DetailViewController.h
//  Bilbe application
//
//  Created by Rahul kumar on 11/20/13.
//  Copyright (c) 2013 Vmoksha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Verse.h"
#import "SearchManager.h"
#import "AutoCompleteManager.h"

@protocol DetailViewControllerDelegate <NSObject>

- (void)searchVersesForKey:(NSString *)searchQuery;

@end

@interface DetailViewController : UIViewController < NSLayoutManagerDelegate, UIScrollViewDelegate, UITextViewDelegate, UITextFieldDelegate, SearchManagerDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *detailView;
@property (weak, nonatomic) IBOutlet UIButton *scrollUpButton;
@property (weak, nonatomic) IBOutlet UIButton *scrollDownButton;

@property (weak, nonatomic) id<DetailViewControllerDelegate> delegate;
@property (strong, nonatomic) Verse *verse;
- (IBAction)shareBtnAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *searchButtonAction;
- (IBAction)actionSearchButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;


@end
