//
//  ModelClass.h
//  Quest For Bible Verses
//
//  Created by Saurabh Suman on 24/07/15.
//  Copyright (c) 2015 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelClass : NSObject


@property(nonatomic,strong)NSString *chapter;
@property(nonatomic,strong)NSString *varses;


@end
