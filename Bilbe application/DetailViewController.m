//
//  DetailViewController.m
//  Bilbe application
//
//  Created by Rahul kumar on 11/20/13.
//  Copyright (c) 2013 Vmoksha. All rights reserved.
//

#import "DetailViewController.h"
#import "BackButton.h"
//#import "Flurry.h"
#import "DatabaseManager.h"

#import "ResultsListedViewController.h"


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface DetailViewController ()
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressForScollDown;
@property (weak, nonatomic) IBOutlet UIImageView *tapToSearchImageView;
@property (weak, nonatomic) IBOutlet UIView *tapToSearchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIView *warningMessageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewTopConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeightConst;
@property (weak, nonatomic) IBOutlet UIControl *searchViewTransperent;
@property (weak, nonatomic) IBOutlet UITableView *autocompleteList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *autocompleteHeightConst;
@property (weak, nonatomic) IBOutlet UIButton *tapToSearchBtnImg;
@property (strong, nonatomic) SearchManager *searchManager;
@property (nonatomic, strong) AutoCompleteManager *autoCompleteManager;

@end

@implementation DetailViewController
{
    NSTimer *timerForScrollingDown, *timerForScrollingUp;
    NSArray *arrayOfKeyWords;
    UIFont *fontForTxtFldWhileEditing;
    BOOL showSearchView;
    NSMutableArray *autoCompleteArray;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    BackButton *backButton = [[BackButton alloc] initWithFrame: CGRectMake(0, 0, 60.0f, 30.0f)];
    
    [backButton addTarget:self action:@selector(backButtonisPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
}

- (void)backButtonisPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
    } else
    {
//        UIButton *buttonForPopoverView = [[UIButton alloc] initWithFrame:(CGRectMake(0, 0, <#CGFloat width#>, <#CGFloat height#>))]
//        self.navigationController.navigationItem.titleView =
    }
    
//    UIImage *tapToSearchImage = [[UIImage imageNamed:@"TapToSearchButton.png"] resizableImageWithCapInsets:(UIEdgeInsetsMake(0, 0, 33, 0))];
    
   // _tapToSearchImageView.image = tapToSearchImage;
    
    self.title = [self.verse.chapter capitalizedString];

    self.scrollDownButton.hidden = YES;
    self.scrollUpButton.hidden = YES;
    
    self.detailView.delegate = self;
    
    fontForTxtFldWhileEditing = self.searchTextField.font;
    self.searchTextField.delegate = self;
    
    showSearchView = NO;
    
    autoCompleteArray = [[NSMutableArray alloc] init];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIFont *fontForTextView ;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        fontForTextView = [UIFont fontWithName:@"Desyrel" size:30];

    } else
        fontForTextView = [UIFont fontWithName:@"Desyrel" size:20];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:1.5];

    
    self.warningMessageView.hidden = YES;
    
    _detailView.textAlignment = NSTextAlignmentNatural;
    _detailView.textColor =[UIColor whiteColor];
    _detailView.font = fontForTextView;
    _detailView.text = self.verse.verse;
    
    self.searchViewTransperent.layer.shadowColor = [UIColor blackColor].CGColor;
    self.searchViewTransperent.layer.shadowOffset = CGSizeMake(4, 4);
    self.searchViewTransperent.layer.shadowRadius = 2;
    self.searchViewTransperent.layer.shadowOpacity = .8;
    self.searchViewTransperent.layer.masksToBounds = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.detailView.contentSize.height >= self.detailView.bounds.size.height)
    {
        self.scrollDownButton.hidden = NO;
    }
    
    arrayOfKeyWords = [[DatabaseManager sharedInstance] getTheKeyWordsFromDatabase];
}

- (IBAction)shareBtnAction:(UIButton *)sender
{
    
    UIFont *fontForChapters = [UIFont fontWithName:@"Arial-BoldItalicMT" size:18.0];
    UIFont *fontForVerses =[UIFont fontWithName:@"Helvetica Neue" size:12.0];

  //  [Flurry logEvent:@"Share button pressed"];
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    
    NSMutableAttributedString *attributeStrForChapters = [[NSMutableAttributedString alloc] initWithString:self.verse.chapter attributes:@{NSFontAttributeName:fontForChapters,NSParagraphStyleAttributeName:paragraph, NSForegroundColorAttributeName: [UIColor blackColor]}];
    
    NSMutableAttributedString *attributeStrForVerses = [[NSMutableAttributedString alloc] initWithString:self.verse.verse attributes:@{NSFontAttributeName:fontForVerses,NSParagraphStyleAttributeName:paragraph}];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"\n"];
    [attributeStrForChapters appendAttributedString:attributedString];
    
    [attributeStrForChapters appendAttributedString:attributeStrForVerses];
    UIImage *appIconImage = [UIImage imageNamed:@"iconForShare.png"];
    NSMutableAttributedString *attributedAppLink = [[NSMutableAttributedString alloc] initWithString:@"\n\n  Quest for Bible Verse (iPhone/iPad)\n https://itunes.apple.com/in/app/quest-for-bible-verses/id796303032?mt=8"];
    
    [attributeStrForChapters appendAttributedString:attributedAppLink];
    
    NSArray *dataToShare = @[attributeStrForChapters,appIconImage];
    UIActivityViewController *activityView =[[UIActivityViewController alloc]initWithActivityItems:dataToShare applicationActivities:Nil];
    activityView.excludedActivityTypes = @[UIActivityTypePrint,UIActivityTypeCopyToPasteboard];
    
   
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                           NSFontAttributeName: [UIFont fontWithName:@"Arial" size:15]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                           NSFontAttributeName: [UIFont fontWithName:@"Arial" size:15]} forState:(UIControlStateNormal)];
    
    
    activityView.popoverPresentationController.sourceView = self.shareButton;

    [activityView setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        NSShadow *shadow = [[NSShadow alloc] init];
        shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
        shadow.shadowOffset = CGSizeMake(2, 5);
        
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSShadowAttributeName:shadow ,
                                                               NSFontAttributeName: [UIFont fontWithName:@"JamesFajardo" size:33]}];
        
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                          NSShadowAttributeName:shadow ,
                                                                          NSFontAttributeName: [UIFont fontWithName:@"JamesFajardo" size:33]}];
        
        [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor],
                                                               NSFontAttributeName: [UIFont fontWithName:@"JamesFajardo" size:30]} forState:(UIControlStateNormal)];
        
        
        if (completed) {
            // [Flurry logEvent:@"Shareing was sucessfull"];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Posted" message:@"Verses is sent sucessfully" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [alert show];
            
            
            NSLog(@"completed dialog - activity: %@ - finished flag: %d", activityType, completed);
        }
        else
        {
            // [Flurry logEvent:@"Shareing was failure"];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Enter the  email id and send the verses." delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [alert show];
            
        }

    }];
//    [activityView setCompletionHandler:^(NSString *activityType, BOOL completed) {
//        
//    }];
    
    
    [self presentViewController:activityView animated:TRUE completion:nil];
}

- (void)showOrHideScrollButtonsForOffSet:(CGPoint)offset
{
//    NSLog(@"%f", offset.y );
    
    if (offset.y <= 0)
    {
        self.scrollUpButton.hidden = YES;
        [timerForScrollingUp invalidate];

    } else
    {
        if (self.scrollUpButton.hidden)
        {
            self.scrollUpButton.hidden = NO;
        }
    }
    
//    NSLog(@"Content height = %f",self.detailView.contentSize.height);
    if (offset.y >= self.detailView.contentSize.height - self.detailView.frame.size.height)
    {
        self.scrollDownButton.hidden = YES;
        [timerForScrollingDown invalidate];
    } else
    {
        if (self.scrollDownButton.hidden)
        {
            self.scrollDownButton.hidden = NO;
        }
    }
}

- (IBAction)scrollUpButtonAction
{
    CGPoint currentOffset = self.detailView.contentOffset;
    currentOffset.y = currentOffset.y - 30;
    [self.detailView setContentOffset:currentOffset animated:YES];
    [self showOrHideScrollButtonsForOffSet:currentOffset];
}

- (IBAction)scrollDownButtonAction
{
    CGPoint currentOffset = self.detailView.contentOffset;
    currentOffset.y = currentOffset.y + 30;
    
    [self.detailView setContentOffset:currentOffset animated:YES];
    
    [self showOrHideScrollButtonsForOffSet:currentOffset];
    
   // [Flurry logEvent:@"Scroll down button pressed"];
}


- (IBAction)longPressForScrollDownButton:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        timerForScrollingDown = [NSTimer scheduledTimerWithTimeInterval:.2 target:self selector:@selector(scrollDownButtonAction) userInfo:nil repeats:YES];
        [timerForScrollingDown fire];
        
    } else if (sender.state == UIGestureRecognizerStateEnded)
    {
        [timerForScrollingDown invalidate];
    }
}
- (IBAction)longpressForScrollUpButton:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        timerForScrollingUp = [NSTimer scheduledTimerWithTimeInterval:.2 target:self selector:@selector(scrollUpButtonAction) userInfo:nil repeats:YES];
//        [timerForScrollingUp fire];
        
    } else if (sender.state == UIGestureRecognizerStateEnded)
    {
        [timerForScrollingUp invalidate];
    }
}

- (void)showWarningMessageWithAutoCompleteList:(BOOL)hidden
{
    _warningMessageView.hidden = NO;
    [UIView animateWithDuration:.5 animations:^{
        _warningMessageView.alpha = 1;
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.5 delay:1 options:(UIViewAnimationOptionCurveLinear)| UIViewAnimationOptionBeginFromCurrentState animations:^{
            _warningMessageView.alpha = 0;
        } completion:^(BOOL finished) {
            _warningMessageView.hidden = YES;
        }];
        
    }];
    
    _autocompleteList.hidden = hidden;
}

#pragma mark
#pragma Scrollview delegate method
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self showOrHideScrollButtonsForOffSet:self.detailView.contentOffset];
}

#pragma mark
#pragma mark Popview method
- (void)popTheViewWithSearchQuery:(NSString *)searchQuery
{
    self.tapToSearchView.alpha = 0;
    self.autocompleteList.hidden = YES;
    [self.delegate searchVersesForKey:searchQuery];
  //  [self.navigationController popViewControllerAnimated:YES];

  
    CATransition* transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;     //kCATransitionMoveIn;
    
    //kCATransitionFade;  //, , kCATransitionReveal, kCATransitionFade
   
    
    
    transition.subtype =    kCATransitionFromRight;  // kCATransitionFromTop; //kCATransitionFromLeft,, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
   
    



}


#pragma mark
#pragma mark Textfield methods
- (IBAction)returnKeyIsPressed:(UITextField *)sender
{
    [sender resignFirstResponder];
    
    NSDictionary *dictonaryOfResults = [self.searchManager searchResultsForString:_searchTextField.text];
    
    if ([dictonaryOfResults[RETURNVALUEKEY] boolValue])
    {
        _autocompleteList.hidden = YES;
        [self popTheViewWithSearchQuery:dictonaryOfResults[STRINGTOBESENT]];
   
    
        
    
    }

}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.searchTextField.text isEqualToString:@"  Please ask somethig here..."] || [self.searchTextField.text isEqualToString:@"  Please ask something meaningful..."])
    {
        self.searchTextField.textColor = [UIColor blackColor];
        self.searchTextField.font = fontForTxtFldWhileEditing;
        
        [UIView transitionWithView:self.searchTextField duration:.6 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
            self.searchTextField.text = @" ";

        } completion:^(BOOL finished) {
            self.searchTextField.text = @"";
        }];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    _autocompleteList.hidden = NO;
    NSString *subString = [NSString stringWithString:textField.text];
    subString = [subString stringByReplacingCharactersInRange:range withString:string];
    
    [self searchAutocompleteEntriesWithSubstring:subString];
    
    
    if (subString.length == 0)
    {
        _autocompleteList.hidden = YES;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    _autocompleteList.hidden = YES;
    return YES;
}


- (void)managerResultString:(NSString *)result returnValue:(BOOL)returnValue andOccuranceOfString:(BOOL)occurance
{
    if (!returnValue)
    {
        if (occurance)
        {
            self.searchTextField.textColor = [UIColor redColor];
            self.searchTextField.font = [UIFont fontWithName:@"JamesFajardo" size:27];
            
            [UIView transitionWithView:self.searchTextField duration:.6 options:(UIViewAnimationOptionAllowAnimatedContent) animations:^{
                self.searchTextField.text =result;
            } completion:Nil];
            
        }else
        {
            [self showWarningMessageWithAutoCompleteList:YES];
        }
    }
}

- (SearchManager *)searchManager
{
    if (!_searchManager)
    {
        _searchManager = [[SearchManager alloc] initWithArray:arrayOfKeyWords];
        _searchManager.delegate = self;
    }
    return _searchManager;
}

#pragma mark
#pragma  mark Tap To show/hide search view methods
- (IBAction)tapButtOnPressed:(UIButton *)sender
{
    
    if (!showSearchView)
    {
        [self showSearchView];
    }else
    {
        [self hideSearchView];
    }
}
- (IBAction)resignSearchView:(UIControl *)sender
{
    if (!self.autocompleteList.hidden)
    {
        self.autocompleteList.hidden = YES;
    }else if (showSearchView)
    {
        [self hideSearchView];
    }
}

- (void)showSearchView
{

    _tapToSearchBtnImg.hidden=YES;
    

    
    self.searchViewHeightConst.constant = self.view.frame.size.height + 80;
    
    [_searchTextField becomeFirstResponder];
    
    [_tapToSearchBtnImg setBackgroundImage:[UIImage imageNamed:@"Search-Button.png"] forState:UIControlStateNormal];

    
    [UIView animateWithDuration:.3 animations:^{
       
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            // The device is an iPad running iOS 3.2 or later.
       
          _searchViewTopConst.constant = 20;
        }
        else
        {
            // The device is an iPhone or iPod touch.
        
          _searchViewTopConst.constant = -40;
        
        }
        
        
        
      
        
        [self.view layoutIfNeeded];
        _tapToSearchImageView.alpha = 1;
    } completion:^(BOOL finished) {
        
        self.tapToSearchView.alpha = .4;
        [self.tapToSearchView setBackgroundColor:[UIColor blackColor]];
        
    }];
    showSearchView = !showSearchView;

}

- (void)hideSearchView
{
    [self.tapToSearchView setBackgroundColor:[UIColor clearColor]];
    [self.searchTextField resignFirstResponder];


    self.tapToSearchView.alpha = 1;
    self.searchViewHeightConst.constant = 150;
    [UIView animateWithDuration:.3 animations:^{
       
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            // The device is an iPad running iOS 3.2 or later.
            
             _searchViewTopConst.constant = -113;
        }
        else
        {
            // The device is an iPhone or iPod touch.
            
             _searchViewTopConst.constant = -113;
            
        }
  
     
        
        
        
        
        
        [self.view layoutIfNeeded];
        _tapToSearchImageView.alpha = 1;
    } completion:^(BOOL finished) {
        [_tapToSearchBtnImg setBackgroundImage:[UIImage imageNamed:@"Search-Button.png"] forState:UIControlStateNormal];
    }];
    
    showSearchView = !showSearchView;
    _tapToSearchBtnImg.hidden=NO;


}

#pragma mark
#pragma mark Autocompletion List Implementation
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [autoCompleteArray removeAllObjects];
    
    autoCompleteArray = [self.autoCompleteManager searchAutocompleteEntriesWithSubstring:substring];
    
    [self.autocompleteList reloadData];
    [_autocompleteList setContentOffset:(CGPointZero)];
    
    
    //Tableview height according to number of cells
    _autocompleteHeightConst.constant = [self.autoCompleteManager heightOfTableForArrayCount:autoCompleteArray.count andRowHeight:self.autocompleteList.rowHeight];
    
}
- (AutoCompleteManager *)autoCompleteManager
{
    if (!_autoCompleteManager)
    {
        _autoCompleteManager = [[AutoCompleteManager alloc] init];
    }
    return _autoCompleteManager;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return autoCompleteArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"autocompleteID";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    
    NSString *stringForIndex = [[autoCompleteArray objectAtIndex:indexPath.row] string];
    
    if (![stringForIndex isEqualToString:@"No results found..."])
    {
        cell.textLabel.font = [UIFont fontWithName:@"Baskerville-Italic" size:18];
        cell.textLabel.textColor = [UIColor blackColor];
    } else
    {
        cell.textLabel.font = [UIFont fontWithName:@"Baskerville-SemiBoldItalic" size:16];
        cell.textLabel.textColor = [UIColor redColor];
    }
    
    cell.textLabel.attributedText = [autoCompleteArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *stringForIndex = [[autoCompleteArray objectAtIndex:indexPath.row] string];
    
    if (![stringForIndex isEqualToString:@"No results found..."])
    {
        _autocompleteList.hidden = YES;
        
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        //[Flurry logEvent:@"Autocomplete list used"];
        
        self.searchTextField.text = selectedCell.textLabel.attributedText.string;
    }
}

- (IBAction)actionSearchButton:(id)sender {
 
    
    [self returnKeyIsPressed:self.searchTextField];


}
@end
