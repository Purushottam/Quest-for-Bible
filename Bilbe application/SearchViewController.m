//
//  SearchViewController.m
//  Bilbe application
//
//  Created by Rahul kumar on 11/20/13.
//  Copyright (c) 2013 Vmoksha. All rights reserved.
//


#import "SearchViewController.h"
#import "TestAppDelegate.h"
#import "InfoViewController.h"
#import "ResultsListedViewController.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "ParseVOTD.h"
#import "DatabaseManager.h"
//#import "Flurry.h"
#import "AutoCompleteManager.h"

#define URLForVOTD [NSURL URLWithString:@"http://labs.bible.org/api/?passage=votd&type=xml"]

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface SearchViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *serachButton;
@property (weak, nonatomic) IBOutlet UIView *warningMessageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *warnigViewTopConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *warnigViewHeightConst;
@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic, strong) UIPopoverController *popoverViewController;
@property (nonatomic, strong) SearchManager *searchManager;
@property (nonatomic, strong) AutoCompleteManager *autoCompleteManager;

@end

@implementation SearchViewController
{
    NSMutableString *votdChatpter;
    NSMutableString *votdVerse;
    NSString *stringToBeSent;
    UIFont *fontForTxtFldWhileEditing;
    NSMutableArray *autoCompleteArray;
    NSArray *arrayOfKeyWords;
    DatabaseManager *dbmanager;
    UIBarButtonItem *infoBarButton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)loadView
{
    [super loadView];

    UIButton *infoButton;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
         infoButton = [[UIButton alloc] initWithFrame:(CGRectMake(0, 0, 25, 25))];
        [infoButton addTarget:self action:@selector(infoButtonPressed:) forControlEvents:(UIControlEventTouchUpInside)];
    } else
    {
        infoButton = [[UIButton alloc] initWithFrame:(CGRectMake(0, 0, 30, 30))];
        [infoButton addTarget:self action:@selector(infoButtonForIpadPressed:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    
    [infoButton setImage:[UIImage imageNamed:@"Info-icon.png"] forState:(UIControlStateNormal)];
    
    infoBarButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    self.navigationItem.rightBarButtonItem = infoBarButton;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"Animation");
    
    _imageView.animationImages = @[[UIImage imageNamed:@"1.1.png"],[UIImage imageNamed:@"1.2.png"],[UIImage imageNamed:@"2.png"],[UIImage imageNamed:@"3.1.png"],[UIImage imageNamed:@"3.2.png"],[UIImage imageNamed:@"4.1.png"],[UIImage imageNamed:@"4.2.png"],[UIImage imageNamed:@"5.1.png"],[UIImage imageNamed:@"5.2.png"],[UIImage imageNamed:@"6.png"],[UIImage imageNamed:@"7.1.png"],[UIImage imageNamed:@"7.2.png"],[UIImage imageNamed:@"8.png"],[UIImage imageNamed:@"9.1.png"],[UIImage imageNamed:@"9.2.png"],[UIImage imageNamed:@"10.png"],[UIImage imageNamed:@"11.png"],[UIImage imageNamed:@"12.png"],[UIImage imageNamed:@"13.1.png"],[UIImage imageNamed:@"13.2.png"],[UIImage imageNamed:@"14.1.png"],[UIImage imageNamed:@"14.2.png"],[UIImage imageNamed:@"15.png"]];
    
    _imageView.animationDuration = .8;
    _imageView.animationRepeatCount = 1;
    
    [_imageView startAnimating];
    
    dbmanager =[[DatabaseManager alloc]init];
    
    fontForTxtFldWhileEditing = _textFieldForSearching.font;
    _textFieldForSearching.delegate = self;
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        _serachButton.titleLabel.font = [UIFont fontWithName:@"JamesFajardo" size:30];
        
    } else
        _serachButton.titleLabel.font = [UIFont fontWithName:@"JamesFajardo" size:26];
    
    _serachButton.titleLabel.textColor = [UIColor colorWithRed:222/255 green:204/255 blue:126/255 alpha:1];
    _serachButton.titleLabel.shadowColor = [UIColor blackColor];
    
    _serachButton.titleLabel.shadowOffset = CGSizeMake(2, 3);
    
    _textFieldForSearching.layer.shadowColor = [UIColor blackColor].CGColor;
    _textFieldForSearching.layer.shadowOffset = CGSizeMake(2, 2);
    _textFieldForSearching.layer.shadowOpacity = .8;
    _textFieldForSearching.layer.shadowRadius = 5.0;
    _textFieldForSearching.layer.masksToBounds = NO;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if ([UIScreen mainScreen].bounds.size.height == 568)
        {
            _serachFieldTopConst.constant=120;
            _imageViewBottomConst.constant = -330;
        }
        
        if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        {
            _serachFieldTopConst.constant = 20.0f;
            _warnigViewTopConst.constant = 0.0f;
            //            _warnigViewHeightConst.constant = 45;
            
        } else
        {
            _imageView.layer.shadowColor = [UIColor blackColor].CGColor;
            _imageView.layer.shadowOffset = CGSizeMake(3, 3);
            _imageView.layer.shadowOpacity = .8;
            _imageView.layer.shadowRadius = 3.0;
            _imageView.layer.masksToBounds = NO;
        }
    }
    
    
    _activityIndicator.hidden = YES;
    
    self.title = @"Quest For Bible Verses";

    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(reachabilityChanged:)
                                                 name: kReachabilityChangedNotification
                                               object: nil];
    
    NSString *remoteHostName = @"www.google.com";
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
	[self.hostReachability startNotifier];
    
    arrayOfKeyWords = [[DatabaseManager sharedInstance] getTheKeyWordsFromDatabase];
    
    autoCompleteArray = [[NSMutableArray alloc] init];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //    _activityIndicator.hidden = YES;
    
    //    for (NSString *family in [UIFont familyNames])
    //    {
    //        for (NSString *names in [UIFont fontNamesForFamilyName:family])
    //        {
    //            NSLog(@"%@ %@", family, names);
    //        }
    //    }
    
    _warningMessageView.alpha = 0.0f;
    _warningMessageView.hidden = YES;
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    _imageView.animationImages = nil;
}



#pragma mark
- (void)infoButtonPressed:(UIButton *)sender
{
//    TestAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    InfoViewController *infoController = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
    
    [self presentViewController:infoController animated:YES completion:Nil];
}

- (void)infoButtonForIpadPressed:(UIButton *)sender
{
    InfoViewController *infoController = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
    
    if (!_popoverViewController)
    {
        _popoverViewController = [[UIPopoverController alloc] initWithContentViewController:infoController];
    }
    if (_popoverViewController.isPopoverVisible)
    {
        [_popoverViewController dismissPopoverAnimated:YES];
    }else
        [_popoverViewController presentPopoverFromBarButtonItem:infoBarButton permittedArrowDirections:(UIPopoverArrowDirectionUp) animated:YES];
}

- (SearchManager *)searchManager
{
    if(!_searchManager)
    {
        _searchManager = [[SearchManager alloc] initWithArray:arrayOfKeyWords];
        _searchManager.delegate = self;
    }
    return _searchManager;
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    Reachability *reach = [notification object];
    NetworkStatus status = [reach currentReachabilityStatus];
   
    // [self getVOTDFromAPI];
    
    
    if (status == NotReachable)
    {
        UIAlertView *alertForNoNetwork = [[UIAlertView alloc] initWithTitle:@"No Internet Connectivity"  message:@"There is no internet connectvity. Please connect to internet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
       // [alertForNoNetwork show];
    
        
        
               NSTimer *delayForVOTD ;
        
        delayForVOTD =[NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(displayfromDb) userInfo:Nil repeats:NO];
        
       //
    
    } else
    {

        NSTimer *delayForVOTD ;
        delayForVOTD = [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(getVOTDFromAPI) userInfo:Nil repeats:NO];
    }
    

}


-(void)displayfromDb
{
    int r = arc4random_uniform(1000);
    NSDictionary *dictionaryOfVerse = [dbmanager getRandomChapterAndVerse:r];
   [self displaydataintextView:dictionaryOfVerse];
}


- (void)getVOTDFromAPI
{
    NSLog(@"VOTD");
    _verseOfTheDay = [[Verse alloc] init];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:URLForVOTD];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSData *data = (NSData *)responseObject;
        
        ParseVOTD *parserForVODT = [[ParseVOTD alloc] init];
        NSDictionary *dictionaryOfVerse = [parserForVODT getVOTDForData:data];

        
        [self displaydataintextView:dictionaryOfVerse];
        
//
//        _verseOfTheDay.chapter = dictionaryOfVerse[@"chapter"];
//        _verseOfTheDay.verse = dictionaryOfVerse[@"verse"];
//        
//        _activityIndicator.hidden = YES;
//        UIFont *fontForChapters;
//        UIFont *fontForVerses;
//        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
//        {
//            fontForChapters = [UIFont fontWithName:@"JamesFajardo" size:33];
//            fontForVerses =[UIFont fontWithName:@"Desyrel" size:17.0];
//        } else
//        {
//            fontForChapters = [UIFont fontWithName:@"JamesFajardo" size:50];
//            fontForVerses =[UIFont fontWithName:@"Desyrel" size:29];
//        }
//        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
//        paragraph.lineSpacing = 3.0f;
//        paragraph.alignment = NSTextAlignmentCenter;
//        
//        NSMutableAttributedString *attributeStrForChapters = [[NSMutableAttributedString alloc] initWithString:self.verseOfTheDay.chapter attributes:@{NSFontAttributeName:fontForChapters,NSParagraphStyleAttributeName:paragraph, NSForegroundColorAttributeName: [UIColor redColor]}];
//        
//        NSMutableAttributedString *attributeStrForVerses = [[NSMutableAttributedString alloc] initWithString:self.verseOfTheDay.verse attributes:@{NSFontAttributeName:fontForVerses,NSParagraphStyleAttributeName:paragraph}];
//        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"\n"];
//        [attributeStrForChapters appendAttributedString:attributedString];
//        
//        [attributeStrForChapters appendAttributedString:attributeStrForVerses];
//        
//        _VOTDTextView.attributedText = attributeStrForChapters;
//        _VOTDTextView.textColor=[UIColor whiteColor];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    
        
        int r = arc4random_uniform(1000);
        NSDictionary *dictionaryOfVerse = [dbmanager getRandomChapterAndVerse:r];
        [self displaydataintextView:dictionaryOfVerse];
        
    }];
    
    _activityIndicator.hidden = NO;
    [_activityIndicator startAnimating];
    
    [operation start];
}




-(void)displaydataintextView:(NSDictionary *)maindict
{

    
    if ( _verseOfTheDay == nil) {
        
        _verseOfTheDay =[[Verse alloc]init];
    
    }
    
    
    _verseOfTheDay.chapter = maindict[@"chapter"];
    _verseOfTheDay.verse = maindict[@"verse"];
    
    _activityIndicator.hidden = YES;
    UIFont *fontForChapters;
    UIFont *fontForVerses;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        fontForChapters = [UIFont fontWithName:@"JamesFajardo" size:33];
        fontForVerses =[UIFont fontWithName:@"Desyrel" size:17.0];
    } else
    {
        fontForChapters = [UIFont fontWithName:@"JamesFajardo" size:50];
        fontForVerses =[UIFont fontWithName:@"Desyrel" size:29];
    }
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.lineSpacing = 3.0f;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSMutableAttributedString *attributeStrForChapters = [[NSMutableAttributedString alloc] initWithString:self.verseOfTheDay.chapter attributes:@{NSFontAttributeName:fontForChapters,NSParagraphStyleAttributeName:paragraph, NSForegroundColorAttributeName: [UIColor redColor]}];
    
    NSMutableAttributedString *attributeStrForVerses = [[NSMutableAttributedString alloc] initWithString:self.verseOfTheDay.verse attributes:@{NSFontAttributeName:fontForVerses,NSParagraphStyleAttributeName:paragraph}];
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"\n"];
    [attributeStrForChapters appendAttributedString:attributedString];
    
    [attributeStrForChapters appendAttributedString:attributeStrForVerses];
    
    _VOTDTextView.attributedText = attributeStrForChapters;
    _VOTDTextView.textColor=[UIColor whiteColor];

    
    
}





- (IBAction)searchTheQuery:(id)sender
{
    _autocompleteList.hidden = YES;
   // [Flurry logEvent:@"SearchButtonPressed"];
    [_textFieldForSearching resignFirstResponder];
}



-(void)managerResultString:(NSString *)result returnValue:(BOOL)returnValue andOccuranceOfString:(BOOL)occurance
{
    if (!returnValue)
    {
        if (occurance)
        {
            _textFieldForSearching.textColor = [UIColor redColor];
            _textFieldForSearching.font = [UIFont fontWithName:@"JamesFajardo" size:27];
            
            [UIView transitionWithView:self.textFieldForSearching duration:.6 options:(UIViewAnimationOptionAllowAnimatedContent) animations:^{
                _textFieldForSearching.text =result;
            } completion:Nil];

        }else
        {
            [self showWarningMessageWithAutoCompleteList:YES];
        }
    }
}



- (IBAction)resignKeyboard:(id)sender
{
    _autocompleteList.hidden = YES;
    [_textFieldForSearching resignFirstResponder];
}
- (void)showWarningMessageWithAutoCompleteList:(BOOL)hidden
{
    _warningMessageView.hidden = NO;
    [UIView animateWithDuration:.5 animations:^{
        _warningMessageView.alpha = 1;
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:.5 delay:1 options:(UIViewAnimationOptionCurveLinear)| UIViewAnimationOptionBeginFromCurrentState animations:^{
            _warningMessageView.alpha = 0;
        } completion:^(BOOL finished) {
            _warningMessageView.hidden = YES;
        }];
        
    }];
    
    _autocompleteList.hidden = hidden;
}

#pragma mark
#pragma mark Segue methods
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    _autocompleteList.hidden = YES;
    
    if ([segue.identifier isEqualToString:@"searchID"])
    {
        ResultsListedViewController *tableViewController = segue.destinationViewController;
        tableViewController.searchQuery = stringToBeSent;
        tableViewController.keyWord = stringToBeSent;
        
        _textFieldForSearching.text = @"";
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    
    
    NSDictionary *dictionaryOfResults = [self.searchManager searchResultsForString:_textFieldForSearching.text];
    
    stringToBeSent = dictionaryOfResults[STRINGTOBESENT];
    
    if ([dictionaryOfResults[RETURNVALUEKEY] boolValue])
    {
       // [Flurry logEvent:@"Sucessful SearchButtonPress"];
    }
    
    return [dictionaryOfResults[RETURNVALUEKEY] boolValue];
}



#pragma mark
#pragma mark Textfield  methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
   
      _textFieldForSearching.textColor = [UIColor whiteColor];
    
    if ([_textFieldForSearching.text isEqualToString:@"  Please ask something here..."] || [_textFieldForSearching.text isEqualToString:@"  Please ask something meaningful..."])
    {
        _textFieldForSearching.textColor = [UIColor whiteColor];
        
        _textFieldForSearching.font = fontForTxtFldWhileEditing;
        
        [UIView transitionWithView:self.textFieldForSearching duration:.6 options:(UIViewAnimationOptionTransitionCrossDissolve) animations:^{
            _textFieldForSearching.text = @"";
            
        } completion:Nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    _autocompleteList.hidden = NO;
    NSString *subString = [NSString stringWithString:textField.text];
    subString = [subString stringByReplacingCharactersInRange:range withString:string];

    [self searchAutocompleteEntriesWithSubstring:subString];

    
    if (subString.length == 0)
    {
        _autocompleteList.hidden = YES;
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    _autocompleteList.hidden = YES;
    return YES;
}

- (IBAction)returnKeyPressed:(UITextField *)sender
{
    [sender resignFirstResponder];
    
   // [Flurry logEvent:@"Enter button pressed"];
    
    
    NSDictionary *dictionaryOfResults = [self.searchManager searchResultsForString:_textFieldForSearching.text];
    
    stringToBeSent = dictionaryOfResults[STRINGTOBESENT];
    
    if ([dictionaryOfResults[RETURNVALUEKEY] boolValue])
    {
       // [Flurry logEvent:@"Sucessful enter button press"];
        _autocompleteList.hidden = YES;
        [self performSegueWithIdentifier:@"searchID" sender:self];
    }
}


#pragma mark
#pragma mark AutoComlpeteList implimentation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return autoCompleteArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"autocompleteID";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    
    NSString *stringForIndex = [[autoCompleteArray objectAtIndex:indexPath.row] string];
    
    if (![stringForIndex isEqualToString:@"No results found..."])
    {
        cell.textLabel.font = [UIFont fontWithName:@"Baskerville-Italic" size:18];
        cell.textLabel.textColor = [UIColor blackColor];
    } else
    {
        cell.textLabel.font = [UIFont fontWithName:@"Baskerville-SemiBoldItalic" size:16];
        cell.textLabel.textColor = [UIColor redColor];
    }
    
    cell.textLabel.attributedText = [autoCompleteArray objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *stringForIndex = [[autoCompleteArray objectAtIndex:indexPath.row] string];
    
    if (![stringForIndex isEqualToString:@"No results found..."])
    {
        _autocompleteList.hidden = YES;

        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
       // [Flurry logEvent:@"Autocomplete list used"];
        
        
        _textFieldForSearching.text = selectedCell.textLabel.attributedText.string;
    }
}


- (AutoCompleteManager *)autoCompleteManager
{
    if (!_autoCompleteManager)
    {
        _autoCompleteManager = [[AutoCompleteManager alloc] init];
    }
    return _autoCompleteManager;
}

//To get autocomplete results
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring
{
    [autoCompleteArray removeAllObjects];
    
    autoCompleteArray = [self.autoCompleteManager searchAutocompleteEntriesWithSubstring:substring];
    
    [self.autocompleteList reloadData];
    [_autocompleteList setContentOffset:(CGPointZero)];
    
    
    //Tableview height according to number of cells
    _autocompleteHeightConst.constant = [self.autoCompleteManager heightOfTableForArrayCount:autoCompleteArray.count andRowHeight:self.autocompleteList.rowHeight];
    
}


@end
